from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import PyPDF2

print("What is the name of the vendor?")
vendor = raw_input().upper()
#User enters name of the vendor they are looking to buy from
count = 0

text = []

pdfFileObj = open('CertList.pdf', 'rb')
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
pages = pdfReader.numPages
for pageNum in range(0,pages):
    pageObj = pdfReader.getPage(pageNum)
    text.append(pageObj.extractText())
for page in text:
    splitpage = page.splitlines()
    for line in splitpage:
        ratio = fuzz.token_set_ratio(vendor, line)
        if ratio > 80:
            print("That's an ineligible vendor.")
            count += 1
if count == 0:
    print("That's an eligible vendor.")