import xlrd
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

print("What is the name of the vendor?")
vendor = raw_input().upper()
#User enters name of the vendor they are looking to buy from

workbook = xlrd.open_workbook('InelVendors.xls')
worksheet = workbook.sheet_by_name('Ineligible')
#Opens spreadsheet file and specifies what sheet the program should look at
count = 0
#tracks how many times user input matches a value in the spreadsheet

for r in range(worksheet.nrows):
    for c in range(worksheet.ncols):
        #parses worksheet for values in cells
        cell = worksheet.cell(r,c)
        #provides a variable name for each cell value
        ratio = fuzz.token_set_ratio(vendor, cell)
        #determines how well user input matches the value in the cell
        if ratio > 75:
        #chose 75 because it worked for the cases I tested, may need to adjust
                print("That's and ineligible vendor.")
                count += 1
if count == 0:
    print("That's an eligible vendor.")
